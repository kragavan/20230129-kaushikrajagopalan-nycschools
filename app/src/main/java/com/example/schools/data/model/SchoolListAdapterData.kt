package com.example.schools.data.model

/**
 * RecyclerView Adapter containing list of schools and their associated SAT Scores.
 *
 * @property schoolName
 * @property satMathAvgScore
 * @property satCriticalReadingAvgScore
 * @property satWritingAvgScore
 * @property dbn
 */
data class SchoolListAdapterData(
  val schoolName: String? = null,
  val satMathAvgScore: String? = null,
  val satCriticalReadingAvgScore: String? = null,
  val satWritingAvgScore: String? = null,
  val dbn: String? = null,
  ) {
  var isExpanded: Boolean = false
}