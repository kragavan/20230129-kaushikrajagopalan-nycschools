package com.example.schools.data.repository

import com.example.schools.data.network.SchoolListApi

/**
 * Provides the list of schools and SAT scores from datasource
 *
 * @property api
 */
class SchoolListRepository(private val api: SchoolListApi) : BaseRepository(){

    suspend fun getSchools() = safeApiCall {
        api.getSchools()
    }

    suspend fun getScores() = safeApiCall {
        api.getScores()
    }

}