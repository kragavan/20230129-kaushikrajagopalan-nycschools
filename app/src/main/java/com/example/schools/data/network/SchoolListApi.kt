package com.example.schools.data.network

import com.example.schools.data.model.SatScore
import com.example.schools.data.model.School
import retrofit2.http.GET
import retrofit2.http.Headers

/**
 * Retrofit API interface
 *
 */
interface SchoolListApi {

    @GET("s3k6-pzi2.json")
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun getSchools(): List<School>

    @GET("f9bf-2cp4.json")
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json"
    )
    suspend fun getScores(): List<SatScore>
}