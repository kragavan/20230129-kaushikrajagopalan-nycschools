package com.example.schools.data.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Creates a Retrofit instance to make network API calls
 *
 */
class RemoteDataSource constructor() {

    companion object {
        private const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    }

    fun <Api> buildApi(
        api: Class<Api>
    ): Api {

        var loggingInterceptor = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.HEADERS)

        var okHttpClient = OkHttpClient
            .Builder()
            .addInterceptor(loggingInterceptor)
            .build()

        var retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(api)
    }
}