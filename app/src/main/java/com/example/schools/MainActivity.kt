package com.example.schools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.nycschools.R

/**
 * Launch activity. Navigation is controlled by Navigation Components
 *
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}