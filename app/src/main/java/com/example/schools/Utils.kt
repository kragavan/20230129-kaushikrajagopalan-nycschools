package com.example.schools

import android.view.View
import androidx.fragment.app.Fragment
import com.example.schools.data.network.Resource
import com.example.schools.ui.list.SchoolListFragment
import com.google.android.material.snackbar.Snackbar

/**
 * Enable/disable visibility of View objects
 *
 * @param isVisible
 */
fun View.visible(isVisible: Boolean) {
  visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.snackbar(message: String, action: (() -> Unit)? = null) {
  val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_SHORT)
  action?.let {
    snackbar.setAction("Retry") {
      it()
    }
  }
  snackbar.show()
}

/**
 * Handle API error scenarios
 *
 * @param failure [Resource.Failure] object
 */
fun Fragment.handleApiError(
  failure: Resource.Failure
) {

  when {
    failure.isNetworkError -> {
      requireView().snackbar("Internet connection error. Error code ${failure.errorBody}")
    }
    failure.errorCode == 404 -> {
      if (this is SchoolListFragment) {
        requireView().snackbar("Error code: " + failure.errorCode)
      }
    }
    else -> {
      val error = failure.errorBody?.string().toString()
      requireView().snackbar(error)
    }
  }
}