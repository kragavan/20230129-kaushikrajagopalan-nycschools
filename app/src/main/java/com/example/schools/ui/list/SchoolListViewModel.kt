package com.example.schools.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.schools.data.model.SatScore
import com.example.schools.data.model.School
import com.example.schools.data.network.Resource
import com.example.schools.data.repository.SchoolListRepository
import com.example.schools.ui.base.BaseViewModel
import kotlinx.coroutines.launch

/**
 * ViewModel for [SchoolListFragment]
 *
 * @property repository [SchoolListRepository] Repository to get schools
 */
class SchoolListViewModel(val repository: SchoolListRepository) : BaseViewModel(repository) {

    private val _schoolsResponse : MutableLiveData<Resource<List<School>>> = MutableLiveData()
    val schoolsResponse: LiveData<Resource<List<School>>> get() = _schoolsResponse

    private val _scoresResponse : MutableLiveData<Resource<List<SatScore>>> = MutableLiveData()
    val scoresResponse: LiveData<Resource<List<SatScore>>> get() = _scoresResponse


    fun getSchools() {
        viewModelScope.launch {
            _schoolsResponse.value = repository.getSchools()
        }
    }

    fun getScores() {
        viewModelScope.launch {
            _scoresResponse.value = repository.getScores()
        }
    }
}