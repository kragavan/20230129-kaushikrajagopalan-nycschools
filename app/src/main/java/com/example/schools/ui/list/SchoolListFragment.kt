package com.example.schools.ui.list

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.example.nycschools.databinding.FragmentSchoolListBinding
import com.example.schools.data.model.School
import com.example.schools.data.model.SchoolListAdapterData
import com.example.schools.data.network.Resource
import com.example.schools.data.network.SchoolListApi
import com.example.schools.data.repository.SchoolListRepository
import com.example.schools.handleApiError
import com.example.schools.ui.base.BaseFragment
import com.example.schools.visible

/**
 * Displays scrollable list of Schools and their SAT scores
 */
class SchoolListFragment :
  BaseFragment<SchoolListViewModel, FragmentSchoolListBinding, SchoolListRepository>() {
  private val TAG = SchoolListFragment::class.java.simpleName
  private val LAZY_LOAD_LIMIT = 100
  private val MOCK_SCORE = 400
  var schoolListApiResponse = mutableListOf<School>()
  var schoolListAdapterData = mutableListOf<SchoolListAdapterData>()

  var recyclerViewAdapter: SchoolListRecyclerViewAdapter =
    SchoolListRecyclerViewAdapter(schoolListAdapterData)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val linearLayoutManager = LinearLayoutManager(requireContext())
    binding.list.layoutManager = linearLayoutManager
    binding.list.adapter = SchoolListRecyclerViewAdapter(mockSchoolList())
    binding.list.visibility = View.INVISIBLE

    viewModel.schoolsResponse.observe(viewLifecycleOwner) {
      when (it) {
        is Resource.Success -> {
          schoolListApiResponse.addAll(it.value)
          getSatScores()
        }
        is Resource.Failure -> {
          binding.progressbar.visible(false)
          Log.e(TAG, it.toString())
          handleApiError(it)
        }
      }
    }

    binding.apiButton.setOnClickListener {
      binding.progressbar.visible(true)
      viewModel.getSchools()
    }

    binding.scrollUp.setOnClickListener {
      binding.list.smoothScrollToPosition(0)
    }

    binding.list.addOnScrollListener(object: OnScrollListener() {

      override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        //dy is the change in the vertical scroll position
        if(dy > 0){
          //scroll down
          binding.scrollUp.setVisibility(View.GONE)
        }
        else if(dy < 0){
          //scroll up
          binding.scrollUp.setVisibility(View.VISIBLE)
        }
      }

      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        if (newState === RecyclerView.SCROLL_STATE_IDLE) { // No scrolling
          Handler().postDelayed(
            Runnable { binding.scrollUp.setVisibility(View.GONE) },
            2000
          )
        }
      }
    })
    }

  private fun getSatScores() {
    viewModel.getScores()

    viewModel.scoresResponse.observe(viewLifecycleOwner) {

      when (it) {
        is Resource.Success -> {
          Log.d(TAG, "Received SAT scores")

          for (school in schoolListApiResponse) {
            for (score in it.value) {
              if (school.dbn.equals(score.dbn)) {
                val data = SchoolListAdapterData(
                  score.schoolName,
                  score.satMathAvgScore,
                  score.satCriticalReadingAvgScore,
                  score.satWritingAvgScore,
                  score.dbn
                )
                schoolListAdapterData.add(data)
              }
            }
          }

          binding.progressbar.visible(false)

          recyclerViewAdapter =
            SchoolListRecyclerViewAdapter(schoolListAdapterData)
          binding.list.adapter = recyclerViewAdapter
          binding.list.visibility = View.VISIBLE
        }
        is Resource.Failure -> {
          binding.progressbar.visible(false)
          Log.e(TAG, it.toString())
          handleApiError(it)
        }
      }
    }
  }

  override fun getViewModel(): Class<SchoolListViewModel> {
    return SchoolListViewModel::class.java
  }

  override fun getFragmentBinding(
    inflater: LayoutInflater,
    container: ViewGroup?
  ) = FragmentSchoolListBinding.inflate(inflater, container, false)

  override fun getFragmentRepository(): SchoolListRepository {
    return SchoolListRepository(remoteDataSource.buildApi(SchoolListApi::class.java))
  }

  private fun mockSchoolList(): List<SchoolListAdapterData> {
    val dummy = mutableListOf<SchoolListAdapterData>()
    for (i in 0..LAZY_LOAD_LIMIT) {
      val school =
        SchoolListAdapterData(
          schoolName = "Name $i",
          "${MOCK_SCORE + i * 0}",
          "${MOCK_SCORE + i * 0}",
          "${MOCK_SCORE + i * 0}",
          "${i * 10 + 100}"
        )
      dummy.add(school)
    }
    return dummy
  }
}