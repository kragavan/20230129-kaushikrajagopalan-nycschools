package com.example.schools.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.schools.data.repository.BaseRepository
import com.example.schools.data.repository.SchoolListRepository
import com.example.schools.ui.list.SchoolListViewModel
import java.lang.IllegalArgumentException

/**
 * Constructs ViewModel from the ViewModel.class provided to BaseFragment
 *
 * @property repository
 */
class ViewModelFactory(
  private val repository: BaseRepository
): ViewModelProvider.NewInstanceFactory() {

  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    return when {
      modelClass.isAssignableFrom(SchoolListViewModel::class.java) -> SchoolListViewModel(repository as SchoolListRepository) as T
      else -> throw IllegalArgumentException("Class Not found")
    }

  }
}