package com.example.schools.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.databinding.FragmentSchoolDataBinding
import com.example.schools.data.model.SchoolListAdapterData
import com.example.schools.ui.list.SchoolListRecyclerViewAdapter.ViewHolder

/**
 * [RecyclerView.Adapter] to display list of schools, SAT scores returned by [SchoolListViewModel] livedata
 */
class SchoolListRecyclerViewAdapter(
  val schoolListData: List<SchoolListAdapterData>
) : RecyclerView.Adapter<ViewHolder>() {
  private val TAG = SchoolListRecyclerViewAdapter::class.java.simpleName
  private val MATH_SCORE = "Math Score: "
  private val READING_SCORE = "Reading Score: "
  private val WRITING_SCORE = "Writing Score: "

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

    return ViewHolder(
      FragmentSchoolDataBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
      )
    )
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val school = schoolListData[holder.layoutPosition]
    val expanded = school.isExpanded

    holder.schoolName.text = school.schoolName

    holder.mathScore.text = MATH_SCORE + school.satMathAvgScore
    holder.readingScore.text = READING_SCORE + school.satCriticalReadingAvgScore
    holder.writingScore.text = WRITING_SCORE + school.satWritingAvgScore

    // for (score in satScores) {
    //   if (school!!.dbn.equals(score.dbn)) {
    //     holder.mathScore.text = MATH_SCORE + score.satMathAvgScore
    //     holder.readingScore.text = READING_SCORE + score.satCriticalReadingAvgScore
    //     holder.writingScore.text = WRITING_SCORE + score.satWritingAvgScore
    //
    //   }
    // }

    holder.detailsLayout.setVisibility(if (expanded) View.VISIBLE else View.GONE);

    holder.cardView.setOnClickListener {
      val expanded: Boolean = school.isExpanded!!
      school.isExpanded = (!expanded)
      notifyItemChanged(position)
    }
  }

  override fun getItemCount(): Int = schoolListData.size

  inner class ViewHolder(binding: FragmentSchoolDataBinding) :
    RecyclerView.ViewHolder(binding.root) {
    val cardView = binding.cardView
    val schoolName: TextView = binding.name
    val detailsLayout: LinearLayoutCompat = binding.detailsLayout
    val mathScore: TextView = binding.mathScore
    val readingScore: TextView = binding.readingScore
    val writingScore: TextView = binding.writingScore

    override fun toString(): String {
      return super.toString() + " '" + schoolName.text + "'"
    }
  }
}