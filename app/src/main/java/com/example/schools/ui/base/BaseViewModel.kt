package com.example.schools.ui.base

import androidx.lifecycle.ViewModel
import com.example.schools.data.repository.BaseRepository

/**
 * BaseViewModel for ViewModel components
 *
 * @property repository
 */
abstract class BaseViewModel(private val repository: BaseRepository) : ViewModel() {

}