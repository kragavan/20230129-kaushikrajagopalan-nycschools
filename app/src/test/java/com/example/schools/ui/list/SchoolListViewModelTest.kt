package com.example.schools.ui.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.schools.data.model.School
import com.example.schools.data.network.Resource
import com.example.schools.data.repository.SchoolListRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolListViewModelTest {

  @ExperimentalCoroutinesApi
  @get:Rule
  val mainCoroutineRule = InstantTaskExecutorRule()

  private lateinit var viewModel: SchoolListViewModel
  private var repository = mock(SchoolListRepository::class.java)

  // private val testDispatcher = AppDispatchers(
  //   IO = TestCoroutineDispatcher()
  // )

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this);
    viewModel = SchoolListViewModel(repository)
  }

  @Test
  fun testGetSchoolList() = runBlocking {
    val successResponse = generateSuccessResponse()
    Mockito.`when`(repository.getSchools()).thenReturn(successResponse)

    try {
      viewModel.getSchools()
      viewModel.schoolsResponse.getOrAwaitValue()
      Assert.assertEquals(
        successResponse, viewModel.schoolsResponse.value
      )
    } finally {
      // viewModel.schoolsResponse.removeObserver(observer)
    }
  }

  private fun generateSuccessResponse(): Resource.Success<List<School>> {
    val schools = mutableListOf<School>()
    for (i in 0..2) {
      val school = School(dbn = "${100 + i}", schoolName = "Name $i", zip = "10011")
      schools.add(school)
    }
    return Resource.Success(schools)
  }
}