### NYC Schools Data

In this Android app I demonstrate the use of Android MVVM pattern to display a list of NYC schools and their SAT scores.
The list of NYC schools and their SAT scores were referenced from the following links:
1. https://data.cityofnewyork.us/resource/s3k6-pzi2.json
2. https://data.cityofnewyork.us/resource/f9bf-2cp4.json

### Frameworks Used

The app uses the following frameworks to display the schools data

1. Jetpack Navigation component
2. ViewModel, LiveData
3. Coroutines
4. Retrofit - Remote network service

### System Design

The design consists of BaseFragment, BaseRepository and BaseViewModel which provides the boilerplate code for MVVM components

BaseFragment - provides ViewBinding reference. Constructs ViewModel based on the template parameters  
BaseRepository - handles Success, Failure calls from remote network requests
BaseViewModel - Base ViewModel for ViewModels per usecase

SchoolListFragment - Displays list view of Schools and their SAT scores  
SchoolListViewModel - ViewModel provider for SchoolListFragment
SchoolListApi - Retrofit interface to make network calls

The overrall design has been explained further in the diagram below:

![alt text for screen readers](/system_design.png "System Design")


